﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRelyTools.Artifact.Versioning
{
    public interface IVersionReader
    {
        ArtifactVersion Read(string versionString, string prefix = "");
    }
}
