﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRelyTools.Artifact.Versioning
{
    public class i21VersionReader : IVersionReader
    {
        public ArtifactVersion Read(string path, string versionString, string prefix = "i21Installer")
        {
            string major = "", minor = "", build = "", revision = "";
            string root = versionString.Substring(prefix.Length, versionString.Length - prefix.Length);
            string[] parts = root.Split('.');
            major = parts[0];
            minor = parts[1];
            build = parts[2];
            revision = parts[3];
            return new ArtifactVersion()
            {
                Path = path,
                VersionString = root,
                Major = major,
                Minor = minor,
                Build = build,
                Revision = revision
            };
        }

        public ArtifactVersion Read(string versionString, string prefix = "")
        {
            return Read(null, versionString, prefix);
        }
    }
}
