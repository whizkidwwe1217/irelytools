﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iRelyTools.Utils;
using iRelyTools.Artifact.Versioning;
using SevenZipLib;

namespace iRelyTools.Artifact
{
    public class Updater
    {
        private ArtifactVersionComparer comparer;

        public string RemoteDirectory { get; set; }
        public string LocalDirectory { get; set; }
        public ExtractOptions Options { get; set; }

        public ArtifactVersion RecentVersion
        {
            get
            {
                List<ArtifactVersion> builds = GetAvailableBuilds();
                return builds.Last();
            }
        }

        public Updater()
        {
            RemoteDirectory = @"\\fileserver\Projects\i21Web\Installer\16.3\TEST BUILDS";
            LocalDirectory = @"E:\artifacts";
            Options = ExtractOptions.OverwriteExistingFiles;
            comparer = new ArtifactVersionComparer(true);
        }

        public async Task<string> DownloadRecent()
        {
            string path = Path.Combine(LocalDirectory, RecentVersion.VersionString + ".rar");
            await FileUtils.Copy(RecentVersion.Path, path);
            return path;
        }

        public async Task DownloadAndExtractRecentBuild()
        {
            string path = await DownloadRecent();
            string dir = Path.Combine(LocalDirectory, RecentVersion.VersionString);
            if(!Directory.Exists(Path.Combine(LocalDirectory, RecentVersion.VersionString)))
                Directory.CreateDirectory(dir);
            await FileUtils.ExtractRarFile(path, dir, Options);
        }

        public bool DirectoryExists()
        {
            //using (NetworkShareAccess.Access("fileserver"))
            //{
            //    DirectoryInfo di = new DirectoryInfo(DirectoryPath);
            //}
            return Directory.Exists(RemoteDirectory);
        }

        public List<ArtifactVersion> GetAvailableBuilds()
        {
            List<ArtifactVersion> builds = new List<ArtifactVersion>();
            DirectoryInfo di = new DirectoryInfo(RemoteDirectory);
            FileInfo[] files = di.GetFiles("*.rar");
            i21VersionReader reader = new i21VersionReader();
            foreach (FileInfo fi in files)
            {
                ArtifactVersion artifact = reader.Read(fi.FullName, Path.GetFileNameWithoutExtension(fi.FullName), "i21Installer");
                builds.Add(artifact);
            }

            builds.Sort(comparer);
            return builds;
        }

        class ArtifactVersionComparer : IComparer<ArtifactVersion>
        {
            private bool descending;

            public ArtifactVersionComparer(bool descending)
            {
                this.descending = descending;
            }
            public int Compare(ArtifactVersion x, ArtifactVersion y)
            {
                if (descending)
                    return x.IsVersionHigher(y) ? 0 : 1;
                return x.IsVersionHigher(y) ? 1 : 0;
            }
        }
    }
}
