﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using iRelyTools.Artifact;
using iRelyTools.Artifact.Versioning;
using System.IO;

namespace iRelyTools.TestBench
{
    [TestClass]
    public class ArtifactVersionTest
    {
        string versionString = "16.658.0710.8457";
        i21VersionReader reader = new i21VersionReader();

        [TestMethod]
        public void VersionTest()
        {
            ArtifactVersion version = reader.Read(versionString);
            Assert.AreEqual(version.VersionString, "16.658.0710.8457");
        }

        [TestMethod]
        public void VersionMajorTest()
        {
            ArtifactVersion version = reader.Read(versionString);
            Assert.AreEqual(version.Major, "16");
        }

        [TestMethod]
        public void VersionMinorTest()
        {
            ArtifactVersion version = reader.Read(versionString);
            Assert.AreEqual(version.Minor, "658");
        }

        [TestMethod]
        public void VersionBuildTest()
        {
            ArtifactVersion version = reader.Read(versionString);
            Assert.AreEqual(version.Build, "0710");
        }

        [TestMethod]
        public void VersionRevisionTest()
        {
            ArtifactVersion version = reader.Read(versionString);
            Assert.AreEqual(version.Revision, "8457");
        }

        [TestMethod]
        public void VersionGetVersionHigherTest()
        {
            ArtifactVersion version = reader.Read("i21Installer16.21.21.102", "i21Installer");
            ArtifactVersion version2 = reader.Read("i21Installer16.21.21.101", "i21Installer");
            Assert.IsTrue(version.IsVersionHigher(version2));
        }

        [TestMethod]
        public void VersionExceptionTest()
        {
            System.Diagnostics.Debug.WriteLine(Path.Combine("C:\\artifacts", "10.0.0.1"));
        }
    }
}
