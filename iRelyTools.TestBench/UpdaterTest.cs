﻿using iRelyTools.Artifact;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRelyTools.TestBench
{
    [TestClass]
    public class UpdaterTest
    {
        [TestMethod]
        public async Task DownloadRecentTest()
        {
            Updater u = new Updater();
            string downloaded = await u.DownloadRecent();
            Assert.IsTrue(File.Exists(downloaded));
            //Assert.AreEqual(downloaded, @"E:\artifacts\16.21.0906.210.rar");
            System.Diagnostics.Debug.WriteLine(downloaded);
        }

        [TestMethod]
        public async Task ExtractDownloadedBuildTest()
        {
            Updater u = new Updater();
            try
            {
                await u.DownloadAndExtractRecentBuild();
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}
