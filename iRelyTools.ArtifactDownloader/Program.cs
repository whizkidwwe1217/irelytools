﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
namespace iRelyTools.ArtifactDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(System.Environment.CommandLine);

            var charCount = 0;
            int max =10;
            var lines = "The quick brown fox jumps over the lazy dog".Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var str  = lines.GroupBy(w => (charCount += w.Length + 1) / (max + 2))
                        .Select(g => string.Join(" ", g.ToArray()))
                        .ToArray();
            foreach(string s in str)
            {
                Console.WriteLine(s);
            }
        }

        private static void Parse(string[] args)
        {
           
        }
    }
}
