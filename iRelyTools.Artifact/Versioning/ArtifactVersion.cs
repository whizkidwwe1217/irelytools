﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRelyTools.Artifact.Versioning
{
    public class ArtifactVersion
    {
        public string Path { get; set; }
        public string VersionString { get; set; }
        public string Major { get; set; }
        public string Minor { get; set; }
        public string Build { get; set; }
        public string Revision { get; set; }

        public int MajorVersion
        {
            get { return int.Parse(Major); }
        }

        public int MinorVersion
        {
            get { return int.Parse(Minor); }
        }

        public int BuildVersion
        {
            get { return int.Parse(Build); }
        }

        public int RevisionVersion
        {
            get { return int.Parse(Revision); }
        }

        public bool IsVersionHigher(ArtifactVersion otherVersion)
        {
            bool isHigher = false;
            if (RevisionVersion > otherVersion.RevisionVersion)
                isHigher = true;
            if (BuildVersion > otherVersion.BuildVersion)
                isHigher = true;
            if(MinorVersion > otherVersion.MinorVersion)
                isHigher = true;
            if (MajorVersion > otherVersion.MajorVersion)
                isHigher = true;
            return isHigher;
        }
    }
}
