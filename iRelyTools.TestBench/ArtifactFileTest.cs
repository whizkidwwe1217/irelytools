﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using iRelyTools.Artifact;
using iRelyTools.Artifact.Versioning;
using System.Collections.Generic;

namespace iRelyTools.TestBench
{
    [TestClass]
    public class ArtifactFileTest
    {
        [TestMethod]
        public void ArtifactFileExists()
        {
            Updater u = new Updater();
            Assert.IsTrue(u.DirectoryExists(), "The artifact directory does not exists.");
        }

        [TestMethod]
        public void ArtifactGetBuildsTest()
        {
            Updater u = new Updater();
            List<ArtifactVersion> builds = u.GetAvailableBuilds();
            Assert.IsNotNull(builds);
            Assert.IsTrue(builds.Count == 4);
            foreach (ArtifactVersion v in builds)
            {
                System.Diagnostics.Debug.WriteLine(v.VersionString);
            }
        }

        [TestMethod]
        public void ArtifactGetLatestBuild()
        {
            Updater u = new Updater();
            ArtifactVersion artifact = u.RecentVersion;
            Assert.IsNotNull(artifact, "No available artifact.");
            Assert.AreEqual(artifact.VersionString, "16.21.0906.210");
        }
    }
}
