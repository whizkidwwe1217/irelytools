﻿using SevenZipLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iRelyTools.Utils
{
    public sealed class FileUtils
    {
        public static async Task Copy(string src, string dest)
        {
            using (FileStream fss = File.Open(src, FileMode.Open))
            {
                using (FileStream fsd = File.Create(dest))
                {
                    await fss.CopyToAsync(fsd);
                }
            }
        }

        public static async Task CopyFilesAsync(StreamReader Source, StreamWriter Destination)
        {
            char[] buffer = new char[0x1000];
            int numRead;
            while ((numRead = await Source.ReadAsync(buffer, 0, buffer.Length)) != 0)
            {
                await Destination.WriteAsync(buffer, 0, numRead);
            }
        }

        public static async Task ExtractRarFile(string fileName, string targetDir, ExtractOptions options)
        {
            await Task.Run(() =>
            {
                using (SevenZipArchive zip = new SevenZipArchive(fileName, ArchiveFormat.Rar))
                {
                    zip.ExtractAll(targetDir, options);
                }
            });            
        }
    }
}
